import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.addEventListener('keydown', (event) => {
      const { code } = event;

      let winner = null;

      switch (code) {
        case controls.PlayerOneAttack:
          if (secondFighter.health >= 0) {
            secondFighter.health -= getDamage(firstFighter, secondFighter);
            console.log(secondFighter.health);
          }
          break;
        case controls.PlayerTwoAttack:
          if (firstFighter.health >= 0) {
            firstFighter.health -= getDamage(firstFighter, secondFighter);
            console.log(firstFighter.health);
          }
          break;
        case controls.PlayerOneBlock:
          firstFighter.health;
          console.log(firstFighter.health);
          break;
        case controls.PlayerTwoBlock:
          secondFighter.health;
          console.log(secondFighter.health);
          break;
        case controls.PlayerOneCriticalHitCombination:
          firstFighter.health -= criticalDamage(secondFighter);
        case controls.PlayerTwoCriticalHitCombination:
          secondFighter.health -= criticalDamage(firstFighter);
      }
      if (firstFighter.health <= 0) {
        winner = resolve(secondFighter);
      }
      if (secondFighter.health <= 0) {
        winner = resolve(firstFighter);
      }
      console.log(winner);
    });
    console.log('fd');
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() * 2 + 1;

  const power = attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() * 2 + 1;

  const power = defense * dodgeChance;

  return power;
}
export function criticalDamage(fighter) {
  // return criticalDamage
  return fighter.attack * 2;
}
