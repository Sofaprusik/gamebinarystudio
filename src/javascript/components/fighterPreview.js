import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });
  if (fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterElement.append(fighterImg, fighterInfo);
  }
  //attack: 4,
  //defense: 3,

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
export function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const fighterInfo = createElement({
    tagName: 'div'
  });
  const fighterName = createElement({
    tagName: 'h2'
  });
  fighterName.textContent = name;
  const fighterHealth = createElement({
    tagName: 'h2'
  });
  fighterHealth.textContent = health;
  const fighterAttack = createElement({
    tagName: 'h2'
  });
  fighterAttack.textContent = attack;
  const fighterDefense = createElement({
    tagName: 'h2'
  });
  fighterDefense.textContent = defense;
  fighterInfo.append(fighterName, fighterHealth, fighterAttack, fighterDefense);

  return fighterInfo;
}
