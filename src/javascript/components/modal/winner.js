import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
  // call showModal function
  const winner = {
    title: `${fighter.name} is winner`,
    bodyElement: createFighterImage(fighter),
    onClose: () => location.reload()
  };
  showModal(winner);
}
